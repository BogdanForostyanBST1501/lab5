# Лабораторная работа №5 (Графика)

# Задание
1.  Импортировать созданный класс одномерного клеточного автомата.
2.  Создать класс для отображения сменяющихся состояний автомата в виде линий из квадратов разных цветов в зависимости от состояния.
3.  Когда линии достигают нижнего края экрана, они должны начать прокручиваться, при этом старые должны удаляться.
4.  Через интерфейс командной строки должны приниматься: количество ячеек, количество состояний, количество ближайших соседей.
5.  Создать сочитание клавиш для сохранения файла в формате PostScript.

 Исходный код файла automaton_display.py
```python
# -*- coding: utf-8 -*-
#pylint:disable=C0325,W0621,C0103,W0614,W0403,W0401
"""A display class for 1-dimensional cellular automata."""

from __future__ import division
from Tkinter import *
from random import randint
from automaton import CellularAutomaton1D


def get_rand_color():
    """Returns random color in hex format"""
    r = lambda: randint(0, 255)
    return '#%02X%02X%02X' % (r(), r(), r())


class TkAutomaton(object):
    """A display class for 1-dimensional cellular automata."""

    def __init__(self, cells_num=80, states_num=4, nearest_neighbors_num=1):

        # Init
        self.states_num = states_num
        self.nearest_neighbors_num = nearest_neighbors_num
        self.done = 0
        self.paused = 0
        self.text_id = None
        self.text_bg_id = None
        self.lines_on_screen = cells_num
        self.cell_width = 800 / cells_num
        self.cell_height = 600 / self.lines_on_screen
        self.lines = []
        self.automaton = CellularAutomaton1D(cells_num, states_num, nearest_neighbors_num)
        self.automaton.random_initialize()
        self.states_colors = [get_rand_color() for _ in range(self.automaton.states_num)]

        # Create window
        self.tk = Tk()
        self.tk.wm_geometry("800x600+20+40")
        self.title = 'Fun with graphics'
        self.tk.wm_title(self.title)
        self.tk.resizable(width=False, height=False)

        # Bindings
        self.tk.bind("<Escape>", self.finish)
        self.tk.bind("<Control-r>", self.change_pattern)
        self.tk.bind("<Control-s>", self.save_postscript)
        self.tk.bind("<space>", self.pause)
        self.tk.protocol("WM_DELETE_WINDOW", self.finish)

        # Create canvas
        self.canvas = Canvas(self.tk, width=800, height=600)
        self.canvas.pack()

        # First draw
        self.draw_line(self.automaton.get_states())
        self.draw_text()
        self.tk.update()


    def change_pattern(self, e):
        """Randomly changes pattern."""
        self.automaton.transition_table = self.automaton.generate_random_table(
            self.states_num,
            self.nearest_neighbors_num
        )
        self.automaton.random_initialize()
        self.states_colors = [get_rand_color() for _ in range(self.automaton.states_num)]


    def save_postscript(self, e):
        """Saves canvas in postscript format."""
        self.canvas.postscript(file="canvas.ps")


    def finish(self, e=None):
        """Exit from programm."""
        self.done = 1


    def pause(self, e):
        """Pause pattern generating."""
        self.paused = not self.paused
        self.tk.wm_title('[PAUSED] '+self.title)


    def draw_text(self):
        """Draws help message on canvas."""
        self.canvas.delete(self.text_id, self.text_bg_id)
        self.text_bg_id = self.canvas.create_rectangle(10, 10, 200, 115,
                                                       fill='white',
                                                       width=0,
                                                       stipple="gray50")
        help_text = 'Esc - exit\nSpace - pause/continue\nCtrl+s - save\nCtrl+r - change pattern'
        txt = 'Objects count: %i\n%s' % (len(self.canvas.find_all()), help_text)
        self.text_id = self.canvas.create_text(20, 20,
                                               text=txt,
                                               font=('Ubuntu', '12'),
                                               anchor=NW,
                                               activefill='white')


    def draw_line(self, cells):
        """Draws one line of cells."""
        self.lines.append([])
        for i, cell in enumerate(cells):
            x0 = i * self.cell_width
            y0 = (len(self.lines) - 1) * self.cell_height
            x1 = x0 + self.cell_width
            y1 = y0 + self.cell_height
            color = self.states_colors[cell]
            self.lines[-1].append(
                self.canvas.create_rectangle(x0, y0, x1, y1, fill=color, width=0)
            )

        if len(self.lines) <= self.lines_on_screen:
            return
        self.scroll_all_lines()
        self.delete_line()


    def scroll_all_lines(self, positions=1):
        """Scrolls all cells on canvas."""
        for line in self.lines:
            for cell in line:
                self.canvas.move(cell, 0, -1 * self.cell_height * positions)


    def delete_line(self, index=0):
        """Deletes line from canvas with specified index."""
        self.canvas.delete(*self.lines[index])
        self.lines.pop(index)


    def update(self):
        """Loop method."""
        if self.paused:
            return self.tk.update()
        self.tk.wm_title(self.title)
        self.automaton.update()
        self.draw_line(self.automaton.get_states())
        self.draw_text()
        self.tk.update()


if __name__ == '__main__':
    from sys import argv
    try:
        cells_num = int(argv[1])
        states_num = int(argv[2])
        nearest_neighbors_num = int(argv[3])
    except (ValueError, IndexError):
        print '''
        USAGE: python automaton_display.py <c> <s> <n>
            <c> - number of cells
            <s> - number of states
            <n> - number of nearest neighbors
        '''
        exit(1)
    a = TkAutomaton(cells_num, states_num, nearest_neighbors_num)
    while not a.done:
        a.update()